// Insert users
db.users.insertMany([
	{
		firstName: "Chito",
		lastName: "Miranda",
		age: 43,
		contact: {
			phone: "5374589",
			email: "chito@parokya.com"
		},
		courses: ["Python", "React", "PHP"],
		department: "none"
	},

	{
		firstName: "Francis",
		lastName: "Magalona",
		age: 61,
		contact: {
			phone: "73145678",
			email: "francis@email.com"
		},
		courses: ["React", "Laravel", "SASS"],
		department: "none"
	},

	{
		firstName: "Jane",
		lastName: "Doe",
		age: 21,
		contact: {
			phone: "731852741",
			email: "jane@email.com"
		},
		courses: ["React", "Laravel", "SASS"],
		department: "HR"
	},

	{
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact: {
			phone: "98621475",
			email: "neil@email.com"
		},
		courses: ["React", "Laravel", "SASS"],
		department: "HR"
	},

	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact: {
			phone: "2359687",
			email: "stephen@email.com"
		},
		courses: ["React", "Laravel", "SASS"],
		department: "HR"
	}
	])

// Find users with letter s in their first name or d in their last name.
// a. Use the $or operator.
// b. Show only the firstName and lastName fields and hide the _id field.

db.users.find ({
    $or: [
        
        {
            firstName: {$regex: 's', $options: '$i'}
        },
        {
            lastName: {$regex:'d', $options:'$i'}
        },
        
    ]
}, 
{
            firstName: 1,
            lastName: 1,
            _id: 0
})
// Find users who are from the HR department and their age is greater than or equal to 70
// a. Use the $and operator

db.users.find({
	$and: [
		{
			department: "HR" 
		},
		{
			age: { $gte: 70 }
		}
	]
})

// Find users with the letter e in their first name and has an age of less than or equal to 30.
// a. Use the $and, $regex and $lte operators

db.users.find({
	$and: [
		{
			firstName: { $regex: "e" }
		},
		{
			age: { $lte: 30 }
		}
	]
})